<!DOCTYPE html>
<html>
<head>
<title>Reward Management System</title>
<script>
	function giveRewardFunc()
		{
			var instructorID = document.getElementById("instructor_id").value;
			var studentID = document.getElementById("student_id").value;
			var ruleID = document.getElementById("rule_id").value;
			var giveReward = document.getElementById("giveReward").value;
		
			if(instructorID != "")
			{
				var xmlhttp=new XMLHttpRequest();
				
				xmlhttp.onreadystatechange=function()
				{
					if (xmlhttp.readyState==4 && xmlhttp.status==200)
					{
						document.getElementById("showResponse").innerHTML=xmlhttp.responseText;
					}
				}
				xmlhttp.open("GET","client.php?giveReward="+giveReward+
																"&rule_id="+ruleID+
																"&student_id="+studentID+
																"&instructor_id="+instructorID,true);
				
				xmlhttp.send();
			}
			else alert("please fill in your InstructorID");
		}
</script>

</head>

<body>
		
		Instructor ID: <input type="text" id="instructor_id" /><br>
		Student ID: <input type="text" id="student_id" value="<?php echo $_POST['getID']?>" readonly="readonly" /><br>
		Rule Name:
		<?php 
			$con=mysqli_connect("localhost","root","root","rewarddatabase");
			
			// Check connection
			if (mysqli_connect_errno()){
				return "Failed to connect to MySQL: " . mysqli_connect_error();
			}
			
			$query= "SELECT Rule_ID,Rule_Name FROM rules";
			$result = mysqli_query($con,$query);
			mysqli_close($con);
			echo "<select id = 'rule_id'>";
				while($row = mysqli_fetch_array($result))
				{
					echo "<option value=".$row['Rule_ID'].">".$row['Rule_Name']."</option>";
				}
			echo "</select>"; 
		?>
		<br>
		<input type="submit" id="giveReward" value="giveReward" onclick="giveRewardFunc()" />
		<div id="showResponse" />
</body>

</html>