<?php

require_once "lib/nusoap.php";

function createConnection($host,$username,$password,$dbname){
	// Create connection
	$con=mysqli_connect($host,$username,$password,$dbname);

	// Check connection
	if (mysqli_connect_errno()){
		return "Failed to connect to MySQL: " . mysqli_connect_error();
	}
	return $con;
}

function closeConnection($con){
	mysqli_close($con);
}

///////// save transaction into reward table///////////////
function giveReward($instructorId,$studentId,$ruleId){
	
	$con = createConnection("localhost","root","root","rewarddatabase");
	
	$date = date('Y-m-d H:i:s');
	
	$query="INSERT INTO rewards (Student_ID, Instructor_ID, Rule_ID, Time_Stamp)
				VALUES (".$studentId.",".$instructorId.",".$ruleId.",'".$date."')";
	 
				 
	$result = mysqli_query($con,$query);
	
	closeConnection($con);
	return "The reward is given.";
}

////////// search ////////////////////////////////
function searchInfo($searchBy,$keyword){
	
	$con = createConnection("localhost","root","root","rewarddatabase");
	
	switch ($searchBy) {
    case "studentID":
		$query= "SELECT users.User_ID,users.User_Name,users.User_Surname,students.Bann_ID 
		FROM students,users 
		WHERE students.Student_ID = $keyword AND students.Student_ID = users.User_ID";	
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
    case "name":
        $query= "SELECT users.User_ID,users.User_Name,users.User_Surname,students.Bann_ID
		FROM students,users
		WHERE users.User_Name =  '$keyword' AND students.Student_ID = users.User_ID";	
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
    case "surname":
		$query= "SELECT users.User_ID,users.User_Name,users.User_Surname,students.Bann_ID
		FROM students,users
		WHERE users.User_Surname = '$keyword' AND students.Student_ID = users.User_ID";
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
	case "bann":
		$query= "SELECT users.User_ID,users.User_Name,users.User_Surname,students.Bann_ID
		FROM students,users
		WHERE Bann_ID = $keyword AND students.Student_ID = users.User_ID";	
		$result = mysqli_query($con,$query);
		closeConnection($con);
		$table = showResult($result);		// Comment this line to disable displayRule
		return $table;
        break;
	}
	
	
	
	
}

function showResult($result){
		
		$table ="<form action='giveReward.php' method='post'>
		<table border='1'>
		<tr>
		<th/>
		<th>Student_ID</th>
		<th>Name</th>
		<th>Surname</th>
		<th>Bann_ID</th>
		</tr>";
		
		
		while($row = mysqli_fetch_array($result))
		  {
			  $table .= "<tr>";
			  $table .= "<td><input type='radio' name='getID' value ='".$row['User_ID']."' checked></td>";
			  $table .= "<td>" . $row['User_ID'] . "</td>";
			  $table .= "<td>" . $row['User_Name'] . "</td>";
			  $table .= "<td>" . $row['User_Surname'] . "</td>";
			  $table .= "<td>" . $row['Bann_ID'] . "</td>";
			  $table .= "</tr>";
		  }
		$table .= "</table>
		
		<input type='submit' name='choose' value='Choose'>
		</form>";
		
	return $table;
}



// create object to deal with service provider
$server = new soap_server();
$server->register("giveReward");
$server->register("searchInfo");


if(!isset($HTTP_RAW_POST_DATA))
	$HTTP_RAW_POST_DATA = file_get_contents('php://input');

$server->service($HTTP_RAW_POST_DATA);

?>